/*
 * Copyright 2016 M-Way Solutions GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.relution.jenkins.awssqs.model;

import com.amazonaws.services.sqs.model.Message;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.relution.jenkins.awssqs.interfaces.MessageParser;
import io.relution.jenkins.awssqs.logging.Log;
import io.relution.jenkins.awssqs.model.entities.codecommit.ExecuteJenkinsJobEvent;
import io.relution.jenkins.awssqs.model.entities.codecommit.MessageBody;
import io.relution.jenkins.awssqs.model.entities.codecommit.SessionCamTestEvent;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;


public class SessionCamTestMessageParser implements MessageParser {

    private static final String EVENT_SOURCE_CODECOMMIT = "sessioncamtest";
    private static final String EVENT_SOURCE_SESSIONCAM = "sessioncamtest";

    private final Gson gson;

    public SessionCamTestMessageParser() {
        this.gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    @Override
    public List<ExecuteJenkinsJobEvent> parseCodeCommitMessage(Message message) {
        return null;
    }

    @Override
    public List<SessionCamTestEvent> parseMessage(final Message message) {
        try {

            final String messageBody = message.getBody();
            final MessageBody body = this.gson.fromJson(messageBody, MessageBody.class);
            Log.info("Got message with subject: %s", body.getSubject());
            final Map<String, String> jsonCoreDetails = body.getOfflineCoreRequestDetails();

            Log.warning("json Core Details:" +jsonCoreDetails);

            for (Map.Entry<String, String> entry : jsonCoreDetails.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                Log.warning("Key: %s, Value: %s", key, value);
            }

            if (StringUtils.isEmpty(message.toString())) {
                Log.warning("Message contains no text");
                return Collections.emptyList();
            }

            if (!messageBody.startsWith("{") || !messageBody.endsWith("}")) {
                Log.warning("Message text is no JSON");
                return Collections.emptyList();
            }

            return Collections.singletonList(this.gson.fromJson(messageBody, SessionCamTestEvent.class));
//            return "";//this.parseTestRecords(json);
            //todo:  Commented this out from test case
//            String body = message.getBody();
//            io.relution.jenkins.awssqs.logging.Log.info("Found json body: '%s'", body);
//            return Collections.singletonList(this.gson.fromJson(body, SessionCamTestEvent.class));
////            Log.info("Got message with subject: %s", body.getSubject());
////            final String json = body.getMessage();
////            Log.info("Body of the message: %s", json);
////            if (StringUtils.isEmpty(json)) {
////                Log.warning("Message contains no text");
////                return Collections.emptyList();
////            }
////
////            if (!json.startsWith("{") || !json.endsWith("}")) {
////                Log.warning("Message text is no JSON");
////                return Collections.emptyList();
////            }
//////            return new ArrayList<Event>();
////            return this.parseRecords(json);
        } catch (final com.google.gson.JsonSyntaxException e) {
            io.relution.jenkins.awssqs.logging.Log.warning("JSON syntax exception, cannot parse message: %s", e);
        }
        return Collections.emptyList();
    }

//    private List<Event> parseTestRecords(final String json) {
//        final Records records = this.gson.fromJson(json, Records.class);
//        final List<Event> events = new ArrayList<>(records.size());
//
//        for (final Record record : records) {
//            this.parseEvents(events, record);
//        }
//
//        return events;
//    }

//    private List<ExecuteJenkinsJobEvent> parseRecords(final String json) {
//        final ExecuteJenkinsJobEvent records = this.gson.fromJson(json, ExecuteJenkinsJobEvent.class);
//        return Collections.singletonList(records);
//
////        for (final Record record : records) {
////            this.parseEvents(events, record);
////        }
////
////        return events;
//    }

//    private void parseEvents(final List<io.relution.jenkins.awssqs.interfaces.Event> events, final io.relution.jenkins.awssqs.model.entities.codecommit.Record record) {
//        if (!this.isSessionCamEvent(record)) {
//            return;
//        }
//
//        final io.relution.jenkins.awssqs.model.entities.codecommit.SessionCamTest sessionCamTest = record.getSessionCamTest();
//
//        for (final io.relution.jenkins.awssqs.model.entities.codecommit.Reference reference : sessionCamTest.getReferences()) {
//            final io.relution.jenkins.awssqs.interfaces.Event event = new io.relution.jenkins.awssqs.model.entities.codecommit.SessionCamTestEvent(record, reference);
//            events.add(event);
//        }
//    }

    private boolean isSessionCamEvent(final io.relution.jenkins.awssqs.model.entities.codecommit.Record record) {
        return StringUtils.equals(EVENT_SOURCE_SESSIONCAM, record.getEventSource());
    }

    private boolean isCodeCommitEvent(final io.relution.jenkins.awssqs.model.entities.codecommit.Record record) {
        return StringUtils.equals(EVENT_SOURCE_CODECOMMIT, record.getEventSource());
    }
}
