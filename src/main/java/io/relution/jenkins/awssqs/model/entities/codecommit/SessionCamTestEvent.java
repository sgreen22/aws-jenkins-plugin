/*
 * Copyright 2016 M-Way Solutions GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.relution.jenkins.awssqs.model.entities.codecommit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SessionCamTestEvent {
//
//    private final static String HOST = "sessioncam.com";
//    private final static String PATH = "/tmp/somepath";

    @Expose
    @SerializedName("jobname")
    private String jobName;

    public String getJobName() {
        return jobName;
    }

    @Expose
    @SerializedName("corename")
    private String corename;

    public String getCoreName() {
        return this.corename;
    }

//   {
//"Subject": "Some-subject",
//        "Message": "Some-message",
//        "jobname" : "Solr_Offline_Core",
//        "EventSource": "sessioncamtest",
//        "OfflineCoreRequest": {
//        "RequestId": "7d4bb983-7850-474c-a9ce-b6c22a6fd848",
//                "Corename": "20170311_A",
//                "RequestDataS3Path": "sessioncam-live-exportdata/a/07-04-2017/{Hostname}/{RequestId}"
//    }
//}


}
